import os
import math
from qgis.core import (
  QgsGeometry,
  QgsPoint,
  QgsPointXY,
  QgsWkbTypes,
  QgsProject,
  QgsFeatureRequest,
  QgsVectorLayer,
  QgsDistanceArea,
  QgsUnitTypes,
)

##Dado o tamanho da imagem em pixeis e o sensor width calcula o sensor height
def getSensorH (sw, wph, hph):
    return (hph * (sw / wph))

##Dadas duas geometrias ve se existe intersecao entre elas 
##Usado para ver se uma fotografia e o terreno a cobrir se intercetam
def check (rec, poligono):
    return rec.intersection(poligono).isEmpty()

    
## Dado um sensor, uma altura, um fl, e um overlap, calcula quanto o drone precisa de andar para o lado
def distanciafoto (altura, sensor, fl):
    return (sensor / fl) * altura

##Dado um poligono e duas distancias (calculadas atraves do overlap, sendo uma no sentido 
##do vetor e a outro no sentido perpendicular ao vetor) retorna todos os pontos 
##possiveis nesse poligono
def geraPontos (direcao, rect, distanciaA, distanciaB):
        ##Com começamos no ponto (xMin, yMax) caso o angulo seja no segundo quadrante inverte para o quarto. 
        ##Por outras palavras troca a direçao do vetor para o outro polo
        if (direcao >= 90 and direcao <= 180):
            direcao -= 180
        dir = math.radians (direcao)
        ##Calcula cos(angle) sin(angle)
        xdir = math.cos (dir)
        
        ydir = math.sin (dir)
        
        ##Define os limites um bocado fora da zona de voo
        aux = max (distanciaA, distanciaB)
        xmax = rect.xMaximum() + aux
        xmin = rect.xMinimum() - aux
        ymax = rect.yMaximum() + aux
        ymin = rect.yMinimum() - aux
        

        ##Cria uma lista para armazenar os pontos
        pontinhos = []
        ##Armazena o primeiro ponto, e passa para o proximo
        ponto = [xmin, ymax]
        pontinhos.append ([ponto[0],ponto[1]])

        ponto[0] += xdir * distanciaA
        ponto[1] += ydir * distanciaA
        ##Enquanto os pontos nao estiverem totalmente fora do rectangulo de voo
        while ((ponto[0] >= xmin and ponto[0] <= xmax) or (ponto[1] >= ymin and ponto[1] <= ymax)):
            ##Caso tenha saido do eixo do X, guarda o ponto, desloca-se com o overlay
            ## perpendicular ao vetor e inverte o sentido do vetor
            if ((ponto[0] > xmax) or ponto[0] < xmin):
                pontinhos.append (ponto)
                #Se estiver a "subir" aumenta o Y, se estiver a "descer" diminui
                if (ydir > 0):
                    ponto[1] += abs(xdir) * distanciaB
                if (ydir <= 0 ):
                    ponto[1] -= abs(xdir) * distanciaB
                #Inverte o sentido do vetor    
                xdir = -xdir
                ydir = -ydir
                
            ##Mesma situaçao que o if de cima, mas desta vez caso saia do eixo dos Y    
            if ((ponto[1] > ymax ) or (ponto[1] < ymin)):
                pontinhos.append ([ponto[0],ponto[1]])
                if (xdir > 0):
                    ponto[0] += abs(ydir) * distanciaB
                if (xdir <=0):
                    ponto[0] -= abs(ydir) * distanciaB
                    

                xdir = -xdir
                ydir = -ydir
                flag = 1
            ##Adiciona o ponto à lista, e prepara o proximo ponto para ser tratado
            pontinhos.append ([ponto[0],ponto[1]])
            ponto[0] += xdir * distanciaA
            ponto[1] += ydir * distanciaA
        return (pontinhos)
        
            
            
        

#Carrega as layers com os nomes pretendido (neste caso pega na primeira)
layer = QgsProject.instance().mapLayersByName("teste")[0]

##Pega nos poligonos da layer
features = layer.getFeatures()

##trata de cada um dos poligonos da layer
for f in features:
    #Cria uma lista de atributos de f
    g = f.attributes()
    
    #Ve se a direçao é null
    if (g[7] == NULL): 
        g[7] = 0
    
    #Modulo 360
    g[7] = g[7]%360
    
    #Poe os atributos num dicionario
    dados = {
        "overlap": g[1],
        "widthsen": g[2],
        "flenght": g[3],
        "widthph": g[4],
        "heighph": g[5],
        "altura": g[6],
        "dir": g[7]
    }
    

    
    #Cria o rectangulo possivel de voo    
    rect = f.geometry().boundingBox()


    #Calcula sensor Height da camera
#    aux = getSensorH (g[2],g[4],g[5])
    #Calcula o Widht e o Height do chao capturado na foto
#    dw = distanciafoto (g[6], g[2], g[3])
#    dh = distanciafoto (g[6], aux , g[3])
    ##Calcula o overlap em metros
#    woverlap = dw * (100-g[1]) / 100
#    hoverlap = dh * (100-g[1]) / 100
#    pontos = geraPontos (90, rect, woverlap, hoverlap)
    dh = 10
    dw = 10
    pontos = geraPontos (0, rect, 3, 3)
    
    ##Cria uma lista finalpontos para receber os pontos que apanham algo pretendido
    finalPontos = []
    
    ##Analisa ponto a ponto dos pontos todos do rectangulo
    for pontinho in pontos:
        ##Prepara para gerar um rectangulo (equivalente à fotografia
        pxmax = pontinho[0] + dw/2
        pxmin = pontinho[0] - dw/2
        pymax = pontinho[1] + dh/2
        pymin = pontinho[1] - dh/2
        ##Gera o rectangulo
        reccheck = QgsGeometry.fromPolygonXY([[QgsPointXY(pxmin, pymin), QgsPointXY(pxmax, pymin), QgsPointXY (pxmax, pymax), QgsPointXY(pxmin, pymax)]])
        ##Verifica se o rectangulo apanha parte do poligono pretendido
        ##Se apanhar (isEmpty == FALSE) adiciona o ponto à lista FinalPoints
        if (check (reccheck,f.geometry()) == False):
            finalPontos.append (QgsPointXY(pontinho[0],pontinho[1]))

    
    
    
    #Esta macacada dá print
    if (len(QgsProject.instance().mapLayersByName('poly')) == 0):
        layer =  QgsVectorLayer('Polygon?crs=EPSG:3763', 'poly' , "memory")
        pr = layer.dataProvider() 
        poly = QgsFeature()
        poly.setGeometry(QgsGeometry.fromRect(rect))
        pr.addFeatures([poly])
        layer.updateExtents()
        QgsProject.instance().addMapLayers([layer])
        
        
    if (len(QgsProject.instance().mapLayersByName('points')) == 0):
        layer =  QgsVectorLayer('Point?crs=EPSG:3763', 'points' , "memory")
        pr = layer.dataProvider()
        points = QgsFeature()
        for h in finalPontos:
            points.setGeometry(QgsGeometry.fromPointXY(h))
            pr.addFeatures([points])
        layer.updateExtents()
        QgsProject.instance().addMapLayers([layer])
#        